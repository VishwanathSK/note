package com.note.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import com.note.utils.TimestampConverter;
import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "notetable")
public class Note implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;

    private String description;

    @TypeConverters(TimestampConverter.class)
    private Date created;

    public Note(String title, String description, Date created) {
        this.title = title;
        this.description = description;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreated() {
        return created;
    }

}
