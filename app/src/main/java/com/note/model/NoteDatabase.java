package com.note.model;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.note.utils.GlobalApplication;


@Database(entities={Note.class},version = 1)
public abstract class NoteDatabase extends RoomDatabase {
    private static NoteDatabase instance;
    public abstract NoteDao noteDao();
    static synchronized NoteDatabase getInstance() {
        if (instance == null) {
            instance = Room.databaseBuilder(GlobalApplication.getAppContext(),
                    NoteDatabase.class, "note_localdb")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
