package com.note.utils;

import android.app.Application;
import android.content.Context;

public class GlobalApplication extends Application {


    private static Context appContext;

    /**
     * If you has other classes that need context object
     * to initialize when application is created,
     * you can use the appContext here to process.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return appContext;
    }
}
