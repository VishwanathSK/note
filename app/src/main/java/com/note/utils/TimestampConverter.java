package com.note.utils;

import android.annotation.SuppressLint;

import androidx.room.TypeConverter;
import com.note.R;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimestampConverter {

    @SuppressLint("SimpleDateFormat")
    private static DateFormat df = new SimpleDateFormat(GlobalApplication
            .getAppContext().getString(R.string.dateformat));
    /**
     * converts date in String format to Date object
     * @param value= String object format is dd-MMM-yyyy hh:mm:ss:S aa
     * @return Date Object
     */
    @TypeConverter
    public static Date fromTimestamp(String value) {
        if (value != null) {
            try {
                //TimeZone timeZone = TimeZone.getTimeZone("IST");
                df.setTimeZone(TimeZone.getDefault());
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    /**
     * converts  Date object to date in String format
     * @param value date object
     * @return String format is dd-MMM-yyyy hh:mm:ss:S aa
     */
    @TypeConverter
    public static String dateToTimestamp(Date value) {
        //TimeZone timeZone = TimeZone.getTimeZone("IST");
        df.setTimeZone(TimeZone.getDefault());
        return value == null ? null : df.format(value);
    }
}
