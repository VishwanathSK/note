package com.note.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.note.R;
import com.note.model.Note;
import com.note.utils.TimestampConverter;
import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {


    private List<Note> notes = new ArrayList<>();
    private Context context;

    NoteAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_layout, parent, false);
        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        Note currentNote = notes.get(position);
        String displaytitle = currentNote.getTitle();
        if(displaytitle.length()>10){
                displaytitle = displaytitle.substring(0,10)+".....";
        }
        holder.textViewTitle.setText(displaytitle);
        holder.textViewcreated.setText(TimestampConverter.dateToTimestamp(currentNote.getCreated()));
        holder.cardView.setOnClickListener(new ClickClass(notes.get(position)));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    class NoteHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewcreated;
        private CardView cardView;
        NoteHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewcreated= itemView.findViewById(R.id.text_view_created);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }

    private class ClickClass implements View.OnClickListener {

        private Note note;
        ClickClass(Note note){
            this.note = note;
        }
        @Override
        public void onClick(View v) {

            if(v.getId()==R.id.cardview){
                Intent intent = new Intent(context,NoteViewActivity.class);
                intent.putExtra("NOTEOBJECT",note);
                context.startActivity(intent);
            }
        }
    }
}
