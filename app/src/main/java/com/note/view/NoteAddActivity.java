package com.note.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.note.R;
import com.note.model.Note;
import com.note.viewmodel.NoteViewModel;

import java.util.Calendar;
import java.util.Objects;

public class NoteAddActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputEditText title,description;
    TextInputLayout titlelayout,desclayout;
    MaterialButton submit;
    ConstraintLayout rootlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_add);
        setUi();
        submit.setOnClickListener(this);

    }

    private void setUi() {
        rootlayout = findViewById(R.id.rootlayout);
        title = findViewById(R.id.TIET_title);
        description = findViewById(R.id.TIET_desc);
        titlelayout = findViewById(R.id.TIL_title);
        desclayout = findViewById(R.id.TIL_desc);
        submit = findViewById(R.id.button_submit);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_submit) {
            if (isValidated()) {
                NoteViewModel noteViewModel = new ViewModelProvider(this)
                        .get(NoteViewModel.class);
                Note note = new Note(
                        Objects.requireNonNull(title.getText()).toString().trim(),
                        Objects.requireNonNull(description.getText()).toString().trim(),
                        Calendar.getInstance().getTime()
                );
                noteViewModel.insert(note);
                Intent intent = new Intent(NoteAddActivity.this, NoteViewActivity.class);
                intent.putExtra("NOTEOBJECT", note);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        }
    }

    private boolean isValidated(){

        if(Objects.requireNonNull(title.getText()).toString().trim().isEmpty()
                || Objects.requireNonNull(description.getText()).toString().trim().isEmpty())
        {
            Snackbar.make(rootlayout,"fields can not be empty",
                    Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
