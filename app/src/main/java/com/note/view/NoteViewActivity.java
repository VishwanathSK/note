package com.note.view;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.note.R;
import com.note.model.Note;
import com.note.utils.TimestampConverter;

public class NoteViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_view);
        if(getIntent()!=null){

            Bundle bundle = getIntent().getExtras();
            assert bundle != null;
            Note note =(Note) bundle.getSerializable("NOTEOBJECT");
            TextView title = findViewById(R.id.titlevalue);
            TextView desc  = findViewById(R.id.descvalue);
            TextView date  = findViewById(R.id.datevalue);
            assert note != null;
            title.setText(note.getTitle());
            desc.setText(note.getDescription());
            date.setText(TimestampConverter.dateToTimestamp(note.getCreated()));

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NoteViewActivity.this,NoteListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP
        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
